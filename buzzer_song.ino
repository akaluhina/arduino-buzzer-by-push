const int pinBuzzer = 10;

void setup() {
 pinMode(pinBuzzer, OUTPUT);
}

void loop() {
tone(pinBuzzer, 700, 300);
delay(600);
tone(pinBuzzer, 700, 300);
delay(600);
tone(pinBuzzer, 780, 150);
delay(300);
tone(pinBuzzer, 700, 150);
delay(300);
tone(pinBuzzer, 625, 450);
delay(600);
tone(pinBuzzer, 590, 150);
delay(300);
tone(pinBuzzer, 520, 150);
delay(300);
tone(pinBuzzer, 460, 450);
delay(600);
tone(pinBuzzer, 350, 450);
delay(1200);
tone(pinBuzzer, 350, 450);
delay(600);
tone(pinBuzzer, 460, 450);
delay(600);
tone(pinBuzzer, 520, 150);
delay(300);
tone(pinBuzzer, 590, 150);
delay(300);
tone(pinBuzzer, 625, 450);
delay(600);
tone(pinBuzzer, 590, 150);
delay(300);
tone(pinBuzzer, 520, 150);
delay(300);
tone(pinBuzzer, 700, 1350);
delay(1000);
 } 
